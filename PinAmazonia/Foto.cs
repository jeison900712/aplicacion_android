﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia.Models
{
    public class Foto
    {
        private String contenido;
        private Perfil fk_perfil;

        public string Contenido {
            get { return contenido; }
            set { contenido = value; }
        }

        public Perfil FK_perfil {
            get { return fk_perfil; }
            set { fk_perfil = value; }
        
        }
        public Foto() { }

        public Foto(String contenido, Perfil fk_perfil) {
            FK_perfil = fk_perfil;
            Contenido = contenido;
        
        }
    }
}