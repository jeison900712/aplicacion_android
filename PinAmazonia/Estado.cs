﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia.Models
{
    public class Estado
    {
        private String contenido;
        private Perfil fk_perfil;

        public string Contenido
        {
            get { return contenido; }
            set { contenido = value; }
        }
        public Perfil Fk_perfil
        {
            get { return fk_perfil; }
            set { fk_perfil = value; }
        }

        public Estado() { }

        public Estado(String contenido, Perfil fk_perfil) {
            Contenido = contenido;
            Fk_perfil = fk_perfil;
        }


    }
}