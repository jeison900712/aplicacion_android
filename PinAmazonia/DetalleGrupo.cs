﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PinAmazonia.Models
{
    public class DetalleGrupo
    {
        private String fechagg;
        private Grupo fk_grupo;
        private Usuario fk_usuario;

        public string Fechagg
        {
            get { return fechagg; }
            set { fechagg = value; }

        }
        public Grupo Fk_grupo
        {
            get { return fk_grupo; }
            set { fk_grupo = value; }
        }
        public Usuario Fk_usuario
        {
            get { return fk_usuario; }
            set { fk_usuario = value; }
        }

       
        public DetalleGrupo() { }

        public DetalleGrupo(String fechagg, Grupo fk_grupo, Usuario fk_usuario) {
            Fechagg = fechagg;
            Fk_grupo = fk_grupo;
            Fk_usuario = fk_usuario;
        
        }


    }
}