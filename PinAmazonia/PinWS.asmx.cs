﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PinAmazonia
{
    /// <summary>
    /// Descripción breve de PinWS
    /// </summary>
    [WebService(Namespace = "http://demo.pinamazonia.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class PinWS : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public Boolean insertar_mensaje(String remi, String desti, String mensa, String archi)
        {
            ManejadorDataContext data = new ManejadorDataContext();
            data.insertar_mensaje(remi, desti, mensa, archi);

            return true;
        }

        [WebMethod]
        public Boolean insertar_foto(String contenido, int fk_perfil)
        {
            ManejadorDataContext data = new ManejadorDataContext();
            data.Proce_foto(contenido, fk_perfil);

            return true;
        }
        [WebMethod]
        public Boolean insertar_Estado(String contenido, int fk_perfil)
        {
            ManejadorDataContext data = new ManejadorDataContext();
            data.Proce_estado(contenido, fk_perfil);

            return true;
        }
        [WebMethod]
        public Boolean insertar_Grupo(String nombre_grupo, int fk_mensaje)
        {
            ManejadorDataContext data = new ManejadorDataContext();
            data.proce_grupo(nombre_grupo, fk_mensaje);

            return true;
        }
        [WebMethod]
        public Boolean insertar_perfil(String nombre_perfil, int fk_user)
        {
            ManejadorDataContext data = new ManejadorDataContext();
            data.proce_perfil(nombre_perfil, fk_user);

            return true;
        }

        [WebMethod]
        public List<Grupo> getgrupo(String nombre_user)
        {

            List<Grupo> lista = new List<Grupo>();

            ManejadorDataContext data = new ManejadorDataContext();

            var resultado = data.listar_grupo(nombre_user);

            foreach (var usuario in resultado)
            {

                lista.Add(new Grupo(

                    usuario.nombre_grupo
                   ));

            }


            return lista;
        }
        [WebMethod]
        public List<Usuario> getusuarios(String nombre_grupo)
        {
            List<Usuario> lista = new List<Usuario>();

            ManejadorDataContext data = new ManejadorDataContext();

            var resultado = data.listar_usuarios2(nombre_grupo);

            foreach (var usuario in resultado)
            {

                lista.Add(new Usuario(

                    usuario.nombre
                   ));

            }
            return lista;
        }
        [WebMethod]
        public List<Mensaje> getmensaje(String remitente, String destinatario)
        {
            List<Mensaje> lista = new List<Mensaje>();

            ManejadorDataContext data = new ManejadorDataContext();

            var resultado = data.listar_mensaje(remitente, destinatario);

            foreach (var usuario in resultado)
            {

                lista.Add(new Mensaje(
                   usuario.fk_remitente,
                    usuario.mensaje

                   ));

            }
            return lista;
        }
        [WebMethod]
        public List<GrupoChat> getgrupochat(String nombre_grupo)
        {
            List<GrupoChat> lista = new List<GrupoChat>();

            ManejadorDataContext data = new ManejadorDataContext();

            var resultado = data.listar_grupo_chat(nombre_grupo);

            foreach (var usuario in resultado)
            {

                lista.Add(new GrupoChat(
                   usuario.nombre_user,
                    usuario.mensaje

                   ));

            }
            return lista;
        }



        [WebMethod]
        public List<InicioSesion> getSesion(String nombre_usuario, String pwd)
        {
            List<InicioSesion> lista = new List<InicioSesion>();

            ManejadorDataContext data = new ManejadorDataContext();

            var resultado = data.iniciar_sesion(nombre_usuario, pwd);

            foreach (var usuario in resultado)
            {

                lista.Add(new InicioSesion(
                   usuario.use_name


                   ));

            }
            return lista;
        }

        [WebMethod]
        public String getprueba(String nombre_usuario, String pwd)
        {

            InicioSesion b = new InicioSesion();
           // String c;
            ManejadorDataContext data = new ManejadorDataContext();
            var resultado = data.iniciar_sesion(nombre_usuario, pwd);


            foreach (var usuario in resultado)
            {


              b = ( new InicioSesion(usuario.use_name));
               
            }

            //if (b.Nombre_usuario != nombre_usuario)
            //{

            //    c = "usuario_invalidado";
            //}
            //else
            //{
            //    c = "usuario_validado";
            //}

            return b.Nombre_usuario;
          

        }







    }
 
}
