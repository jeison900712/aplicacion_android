package com.example.pinamazonia;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TabHost;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;

public class PrincipalActivity extends TabActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.principal);
		TabHost tabhost= getTabHost();
		TabHost.TabSpec ima;
		Intent cambiolayaout;
		Resources res= getResources();
		String perfil =getIntent().getStringExtra("user");
		
		cambiolayaout = new Intent().setClass(this, PerfilActivity.class);
		cambiolayaout.putExtra("user",perfil);
		ima=tabhost.newTabSpec("opcion2").setIndicator("",res.getDrawable(R.drawable.opcion2)).setContent(cambiolayaout);
		tabhost.addTab(ima);
		
		cambiolayaout = new Intent().setClass(this, GruposActivity.class);
		cambiolayaout.putExtra("user",perfil);
		ima=tabhost.newTabSpec("opcion4").setIndicator("",res.getDrawable(R.drawable.opcion4)).setContent(cambiolayaout);
		tabhost.addTab(ima);
		
		cambiolayaout = new Intent().setClass(this, ContactosActivity.class);
		cambiolayaout.putExtra("user",perfil);
		ima=tabhost.newTabSpec("opcion3").setIndicator("",res.getDrawable(R.drawable.opcion3)).setContent(cambiolayaout);
		tabhost.addTab(ima);
		
		cambiolayaout = new Intent().setClass(this, ChatActivity.class);
		cambiolayaout.putExtra("user",perfil);
		ima=tabhost.newTabSpec("opcion1").setIndicator("",res.getDrawable(R.drawable.opcion1)).setContent(cambiolayaout);
		tabhost.addTab(ima);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}

}
