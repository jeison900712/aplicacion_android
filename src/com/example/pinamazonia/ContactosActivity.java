package com.example.pinamazonia;

import java.io.IOException;
import java.util.ArrayList;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ContactosActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactos);
		
		Thread nt = new Thread() {
			String res;
			String [] grupos;
			String nombre_user=getIntent().getStringExtra("user");
			int longitud;
			@Override
			public void run() {

				String NAMESPACE = "http://tempuri.org/";
				String URL = "http://www.ebuhoo.com/chairaws/ServiceChaira.svc?wsdl";
				String METHOD_NAME = "lista_contactos";
				String SOAP_ACTION = "http://tempuri.org/IServiceChaira/lista_contactos";
				
				SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
				request.addProperty("login",nombre_user.toString());
				

				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
						SoapEnvelope.VER11);
				envelope.dotNet = true;

				envelope.setOutputSoapObject(request);

				HttpTransportSE transporte = new HttpTransportSE(URL);

				try {
					transporte.call(SOAP_ACTION, envelope);
					SoapObject resultado = (SoapObject) envelope.getResponse();
					res = resultado.toString();
					res=res.replace("string=", "");
					res=res.replace("anyType", "");
					res=res.replace("{", "");
					res=res.replace("}", "");	
					grupos=res.split(";");
					longitud=resultado.getPropertyCount();

				} catch (HttpResponseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						//Toast.makeText(ContactosActivity.this, res, Toast.LENGTH_LONG).show();
						listar(grupos,longitud);
					}

				});
			}
		};

		nt.start();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contactos, menu);
		return true;
	}
public boolean listar(String[] grupo, int longitud){
		
		ListView lista_grupos=(ListView) findViewById(R.id.list_contactos);
		ArrayList<String> lista=new ArrayList<String>();
		for(int i=0; i <longitud; i++){
			
			lista.add(grupo[i]);
			
		}
		
		ArrayAdapter<String> adaptador =new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
		lista_grupos.setAdapter(adaptador);
		lista_grupos.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Toast.makeText(ContactosActivity.this, "seleccionó", Toast.LENGTH_LONG).show();
				lista_contac();
			}
		});
		return true;
	}
		public void lista_contac(){
			Intent cambiolayaout = new Intent(this, ChatActivity.class);
			startActivity(cambiolayaout);
		}
}
